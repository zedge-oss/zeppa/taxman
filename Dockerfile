FROM debian:stretch
COPY bin/taxman /taxman
ENTRYPOINT "/taxman"
