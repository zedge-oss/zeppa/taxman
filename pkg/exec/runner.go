// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package exec

import (
	"context"
	osexec "os/exec"
)

type Runner interface {
	Run(string, ...string) ([]byte, error)
	RunWithContext(context.Context, string, ...string) ([]byte, error)
}

type RealRunner struct{}

func (r RealRunner) Run(cmd string, args ...string) ([]byte, error) {
	//fmt.Printf("Running: %s %v\n", cmd, args)
	return osexec.Command(cmd, args...).Output()
}

func (r RealRunner) RunWithContext(ctx context.Context, cmd string, args ...string) ([]byte, error) {
	//fmt.Printf("Running: %s %v\n", cmd, args)
	return osexec.CommandContext(ctx, cmd, args...).Output()
}
