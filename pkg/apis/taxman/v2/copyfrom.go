// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import (
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

func (t *Taxonomy) CopyFromModel(src *model.EventTaxonomy) {
	t.AppID = src.AppID
	t.Enums = make([]Enum, len(src.Enums))
	t.Properties = make([]Property, 0)
	for i, enum := range src.Enums {
		t.Enums[i] = Enum{}
		t.Enums[i].CopyFromModel(enum)
	}
	for _, prop := range src.Properties {
		if prop.Lifecycle == model.VoidLifecycle {
			continue // Void life cycle is internal not to be exposed in taxman API
		}
		eventProperty := Property{}
		eventProperty.CopyFromModel(prop)
		t.Properties = append(t.Properties, eventProperty)
	}
}

func (e *Enum) CopyFromModel(src *model.EventEnum) {
	e.Name = src.Name
	e.Fields = make([]EnumField, 0)
	for _, field := range src.Fields {
		if field.Lifecycle == model.VoidLifecycle {
			continue // Void life cycle is internal not to be exposed in taxman API
		}
		enumField := EnumField{}
		enumField.CopyFromModel(field)
		e.Fields = append(e.Fields, enumField)
	}
}

func (f *EnumField) CopyFromModel(src *model.EventEnumField) {
	f.Name = src.Name
	f.Value = &src.Value
}

func (p *Property) CopyFromModel(src *model.EventProperty) {
	p.Name = src.Name
	p.Type = src.Type
	p.Scope = &src.Scope
}
