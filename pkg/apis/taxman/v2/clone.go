// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

func (t Taxonomy) Clone() Taxonomy {
	clone := Taxonomy{
		AppID:      t.AppID,
		Enums:      make([]Enum, len(t.Enums)),
		Properties: make([]Property, len(t.Properties)),
		twin:       t.twin,
	}
	for i := range t.Enums {
		clone.Enums[i] = t.Enums[i].Clone()
	}
	for i := range t.Properties {
		clone.Properties[i] = t.Properties[i].Clone()
	}
	return clone
}

func (e Enum) Clone() Enum {
	clone := Enum{
		Name:   e.Name,
		Fields: make([]EnumField, len(e.Fields)),
	}
	for i := range e.Fields {
		clone.Fields[i] = e.Fields[i].Clone()
	}
	return clone
}

func (f EnumField) Clone() EnumField {
	return EnumField{
		Name:  f.Name,
		Value: f.Value,
	}
}

func (p Property) Clone() Property {
	return Property{
		Name:  p.Name,
		Type:  p.Type,
		Scope: p.Scope,
	}
}
