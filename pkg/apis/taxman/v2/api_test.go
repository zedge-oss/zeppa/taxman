// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import "gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Taxonomy", func() {
	var (
		apiTax Taxonomy
	)

	BeforeEach(func() {
		userScope := model.UserScope
		apiTax = Taxonomy{
			AppID: "foo",
			Enums: nil,
			Properties: []Property{
				{Name: "nonScoped", Type: "String"},
				{Name: "userScope", Type: "Int64", Scope: &userScope},
				{Name: "eventScope", Type: "String", Scope: &eventScope},
				{Name: "internalScope", Type: "Int64", Scope: &internalScope},
				{Name: "envelopeScope", Type: "Int64", Scope: &envelopeScope},
			},
		}
	})

	When("applying defaults", func() {
		var (
			expectedScopes = map[string]model.PropertyScope{
				"nonScoped":     model.EventScope,
				"userScope":     model.UserScope,
				"eventScope":    model.EventScope,
				"internalScope": model.InternalScope,
				"envelopeScope": model.EnvelopeScope,
			}
		)
		BeforeEach(func() {
			apiTax.ApplyDefaults()

		})
		It("should set default scope to Event if property has no scope", func() {
			for _, property := range apiTax.Properties {
				if property.Name == "nonScoped" {
					Expect(*property.Scope).To(Equal(model.EventScope))
				}
			}
		})

		It("should not change scope if scope is set", func() {
			for _, property := range apiTax.Properties {
				Expect(*property.Scope).To(Equal(expectedScopes[property.Name]))
			}
		})
	})
})
