// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	"github.com/fgrosse/zaptest"
	"github.com/gorilla/mux"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/fake"
	typedclientset "gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/typed/dwh/v1beta1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

const (
	fooAppID       = "foo"
	barAppID       = "bar"
	lifescopeAppId = "lifescopes"
)

var _ = Describe("Taxman handlers", func() {
	var (
		h             Handlers
		fakeClientSet *fake.Clientset
		fakeClient    typedclientset.EventTaxonomyInterface
		recorder      *httptest.ResponseRecorder
		request       *http.Request
		handler       http.Handler
		body          io.Reader
		path          = "/taxonomy/foo"
		method        string
		vars          = map[string]string{"appid": ""}
	)
	BeforeEach(func() {
		fakeClientSet = fake.NewSimpleClientset(fooTaxonomy, barTaxonomy, supportedPropertiesTypes)
		fakeClient = fakeClientSet.DwhV1beta1().EventTaxonomies(fakeNamespace)
		h = Handlers{
			Namespace: fakeNamespace,
			Client:    fakeClient,
			Log:       zaptest.LoggerWriter(GinkgoWriter).Sugar(),
		}
	})
	When("handling GET /v2/userproperties", func() {
		JustBeforeEach(func() {
			request = httptest.NewRequest(method, path, body)
			recorder = httptest.NewRecorder()
			request = mux.SetURLVars(request, vars)
			handler = http.HandlerFunc(h.ListUserPropertiesHandler)
			handler.ServeHTTP(recorder, request)
		})
		When("without query arguments", func() {
			var body ListUserPropertiesForTaxonomiesResponse

			JustBeforeEach(func() {
				_ = json.NewDecoder(recorder.Result().Body).Decode(&body)
			})
			It("should return all user properties across all taxonomies", func() {
				Expect(body).To(HaveKeyWithValue(AppID(fooAppID), []Property{
					{"time", "DateTime", nil},
					{"userid", "Int64", nil},
				}))
				Expect(body).To(HaveKeyWithValue(AppID(barAppID), []Property{
					{"time", "DateTime", nil},
					{"userid", "Int64", nil},
				}))
			})
			It("should filter away empty taxonomies", func() {
				Expect(body).To(HaveLen(2))
			})

		})
	})
	When("handling GET /v2/taxonomy", func() {
		JustBeforeEach(func() {
			request = httptest.NewRequest(method, path, body)
			recorder = httptest.NewRecorder()
			request = mux.SetURLVars(request, vars)
			handler = http.HandlerFunc(h.GetTaxonomyHandler)
			handler.ServeHTTP(recorder, request)
		})
		When("/lifescopes (as appid)", func() {
			var msg Taxonomy
			BeforeEach(func() {
				vars["appid"] = lifescopeAppId
			})
			JustBeforeEach(func() {
				_ = json.NewDecoder(recorder.Result().Body).Decode(&msg)
			})

			It("should never share properties with lifecycle Void", func() {
				// Enum with the value Browse is still valid
				Expect(msg.Properties).To(ContainElement(Property{
					Name:  "enum_with_properties_with_void_lifecycle",
					Type:  "Enum:Event",
					Scope: &eventScope,
				}))
				Expect(msg.Properties).To(HaveLen(1))

				// But nothing else, so lets ensure we dont have any lifecycle Void here
				one := 1
				Expect(msg.Enums[0].Fields).To(ContainElement(EnumField{
					Name:  "BROWSE",
					Value: &one,
				}))
				Expect(msg.Enums[0].Fields).To(HaveLen(1))
				Expect(recorder.Code).To(Equal(200))
			})
		})
	})
	When("handling PUT /v2/taxonomy", func() {
		JustBeforeEach(func() {
			method = "PUT"
			request = httptest.NewRequest(method, path, ioutil.NopCloser(strings.NewReader(string(fooApiTaxonomyJson))))
			recorder = httptest.NewRecorder()
			request = mux.SetURLVars(request, vars)
			handler = http.HandlerFunc(h.UpdateTaxonomyHandler)
			handler.ServeHTTP(recorder, request)
		})
		Context("/lifescopes (as appid) with --dry-run=true", func() {
			var msg Taxonomy
			BeforeEach(func() {
				path = "/v2/taxonomy/foo?dry-run=true" // https://github.com/gorilla/mux/issues/167
			})
			JustBeforeEach(func() {
				_ = json.NewDecoder(recorder.Result().Body).Decode(&msg)
			})

			It("should not change taxonomy when dry-run=true", func() {
				body, _ := ioutil.ReadAll(recorder.Body)
				Expect(recorder.Code).To(Equal(200))
				Expect(string(body)).To(Equal("{\"msg\":\"successfully updated (dry run)\"}\n"))
			})
		})
		Context("/lifescopes (as appid) with --dry-run=false", func() {
			var msg Taxonomy
			BeforeEach(func() {
				path = "/v2/taxonomy/foo?dry-run=false" // https://github.com/gorilla/mux/issues/167
			})
			JustBeforeEach(func() {
				_ = json.NewDecoder(recorder.Result().Body).Decode(&msg)
			})

			It("should not change taxonomy when dry-run=true", func() {
				body, _ := ioutil.ReadAll(recorder.Body)
				Expect(recorder.Code).To(Equal(200))
				Expect(string(body)).To(Equal("{\"msg\":\"successfully updated\"}\n"))
			})
		})
	})
})

var (
	lastModTime       = time.Unix(1573217753, 0)
	lastModDateString = lastModTime.Format("2006-01-02T15:04:05Z")
	lastMod           = metav1.NewTime(lastModTime)
	fooTaxonomy       = &v1beta1.EventTaxonomy{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fooAppID,
			Namespace: fakeNamespace,
		},
		Spec: v1beta1.EventTaxonomySpec{
			Taxonomy: model.EventTaxonomy{
				AppID: fooAppID,
				Properties: []*model.EventProperty{
					{Name: "time", Scope: model.InternalScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "DateTime"},
					{Name: "event", Scope: model.EventScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Enum:Event"},
					{Name: "userid", Scope: model.UserScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Int64"},
				},
				Enums: []*model.EventEnum{
					{Name: "Event", Fields: []*model.EventEnumField{
						{Name: "BROWSE", Value: 1, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
						{Name: "CLICK", Value: 2, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
					}},
				},
			},
		},
	}
	barTaxonomy = &v1beta1.EventTaxonomy{
		ObjectMeta: metav1.ObjectMeta{
			Name:      barAppID,
			Namespace: fakeNamespace,
		},
		Spec: v1beta1.EventTaxonomySpec{
			TwinTaxonomy: fooAppID,
			Taxonomy: model.EventTaxonomy{
				AppID: barAppID,
				Properties: []*model.EventProperty{
					{Name: "time", Scope: model.InternalScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "DateTime"},
					{Name: "event", Scope: model.EventScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Enum:Event"},
					{Name: "userid", Scope: model.UserScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Int64"},
				},
				Enums: []*model.EventEnum{
					{Name: "Event", Fields: []*model.EventEnumField{
						{Name: "BROWSE", Value: 1, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
					}},
				},
			},
		},
	}
	supportedPropertiesTypes = &v1beta1.EventTaxonomy{
		ObjectMeta: metav1.ObjectMeta{
			Name:      lifescopeAppId,
			Namespace: fakeNamespace,
		},
		Spec: v1beta1.EventTaxonomySpec{
			Taxonomy: model.EventTaxonomy{
				AppID: lifescopeAppId,
				Properties: []*model.EventProperty{
					{Name: "lifecycle_void", Scope: model.EventScope, Lifecycle: model.VoidLifecycle, LastModified: lastMod, Type: "DateTime"},
					{Name: "enum_with_lifecycle_void", Scope: model.EventScope, Lifecycle: model.VoidLifecycle, LastModified: lastMod, Type: "Enum:Event"},
					{Name: "enum_with_properties_with_void_lifecycle", Scope: model.EventScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Enum:Event"},
				},
				Enums: []*model.EventEnum{
					{Name: "Event", Fields: []*model.EventEnumField{
						{Name: "BROWSE", Value: 1, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
						{Name: "I_HAVE_VOID", Value: 2, Lifecycle: model.VoidLifecycle, LastModified: lastMod},
					}},
				},
			},
		},
	}
	fooApiTaxonomyJson = []byte(`{
      "appid": "foo",
      "properties": [
        {"name": "time", "scope": "Internal", "type": "DateTime", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "event", "scope": "Event", "type": "Enum:Event", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "userid", "scope": "User", "type": "Int64", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
      ],
      "enums": [
        {"name": "Event", "fields": [
          {"name": "BROWSE", "value": 1, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
          {"name": "CLICK", "value": 2, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
        ]}
      ]
    }`)
	barApiTaxonomyJson = []byte(`{
      "appid": "bar",
      "properties": [
        {"name": "time", "scope": "Internal", "type": "DateTime", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "event", "scope": "Event", "type": "Enum:Event", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "userid", "scope": "User", "type": "Int64", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
      ],
      "enums": [
        {"name": "Event", "fields": [
          {"name": "BROWSE", "value": 1, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
          {"name": "BUY", "value": 2, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
        ]}
      ]
    }`)
)
