// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v2

import (
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	"math/rand"
	"reflect"
	"time"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

const (
	appID1 = "app1"
	appID2 = "app2"
)

var (
	lastModToday     = v1.Time{Time: time.Now()}
	lastModYesterday = v1.Time{Time: lastModToday.Time.AddDate(0, 0, -1)}
	userScope        = model.UserScope
	eventScope       = model.EventScope
	internalScope    = model.InternalScope
	envelopeScope    = model.EnvelopeScope
	modelTaxonomy1   = model.EventTaxonomy{
		AppID: appID1,
		Enums: []*model.EventEnum{
			{Name: "Event", Fields: []*model.EventEnumField{
				modelEnumField("A", 1),
				modelEnumField("B", 2),
				modelEnumField("C", 3),
			}},
			{Name: "Enum2", Fields: []*model.EventEnumField{
				modelEnumField("A", 1),
			}},
		},
		Properties: []*model.EventProperty{
			modelProperty("time", "DateTime", internalScope),
			modelProperty("event", "Enum:Event", envelopeScope),
			modelProperty("user_id", "Int64", userScope),
			modelProperty("item_id", "String", eventScope),
			modelProperty("pipeline_internal", "String", internalScope, model.PreProductionLifecycle),
		},
	}
	modelTaxonomy1Twin = model.EventTaxonomy{
		AppID: appID2,
		Enums: []*model.EventEnum{
			{Name: "Event", Fields: []*model.EventEnumField{
				modelEnumField("A", 1),
				modelEnumField("B", 2),
				modelEnumField("C", 3),
				modelEnumField("D", 4),
			}},
		},
		Properties: []*model.EventProperty{
			modelProperty("time", "DateTime", internalScope),
			modelProperty("event", "Enum:Event", envelopeScope),
			modelProperty("user_id", "Int64", userScope),
			modelProperty("item_id", "String", eventScope),
			modelProperty("click_position", "Int16", eventScope),
		},
	}
	apiTaxonomy1 = Taxonomy{
		AppID: appID1,
		Enums: []Enum{
			{Name: "Event", Fields: []EnumField{
				apiEnumField("A", nil),
				apiEnumField("B", nil),
				apiEnumField("C", nil),
			}},
			{Name: "Enum2", Fields: []EnumField{
				apiEnumField("A", nil),
			}},
		},
		Properties: []Property{
			apiProperty("time", "DateTime", &internalScope),
			apiProperty("event", "Enum:Event", &envelopeScope),
			apiProperty("user_id", "Int64", &userScope),
			apiProperty("item_id", "String", &eventScope),
		},
	}
)

var _ = Describe("Converting a taxonomy", func() {
	var (
		modelTax                        *model.EventTaxonomy
		apiTax                          Taxonomy
		numberOfInternalModelProperties int
	)
	BeforeEach(func() {
		modelTax = modelTaxonomy1.DeepCopy()
		apiTax = apiTaxonomy1.Clone()

		for _, property := range apiTax.Properties {
			if *property.Scope == model.InternalScope {
				numberOfInternalModelProperties += 1
			}
		}
	})
	It("should copy AppID", func() {
		Expect(apiTax.AppID).To(Equal(modelTax.AppID))
	})
	It("should copy all the properties", func() {
		for i := range apiTax.Properties {
			Expect(apiTax.Properties[i].SameAs(modelTax.Properties[i])).To(BeTrue())
		}
	})
	It("should copy all the enums", func() {
		Expect(apiTax.Enums).To(HaveLen(len(modelTax.Enums)))
		for i := range apiTax.Enums {
			same := apiTax.Enums[i].SameAs(modelTax.Enums[i])
			Expect(same).To(BeTrue())
		}
	})
})

var _ = Describe("Patching a taxonomy", func() {
	var (
		modelTax *model.EventTaxonomy
		apiTax   Taxonomy
	)
	BeforeEach(func() {
		modelTax = modelTaxonomy1.DeepCopy()
		apiTax = apiTaxonomy1.Clone()
	})
	JustBeforeEach(func() {
		apiTax.PatchInto(modelTax)
		apiTax.BuildIndex()
		modelTax.BuildIndex()
	})
	When("adding a property", func() {
		var lastProp *model.EventProperty
		var scope *model.PropertyScope
		JustBeforeEach(func() {
			apiTax.Properties = append(apiTax.Properties, apiProperty("foo", "String", scope))
			apiTax.PatchInto(modelTax)
			lastProp = modelTax.Properties[len(modelTax.Properties)-1]
		})
		AssertPropertyWasAdded := func() {
			It("should actually be added", func() {
				Expect(modelTax.Properties[5].Name).To(Equal("foo"))
			})
			It("should have its lifecycle set to PreProduction", func() {
				Expect(lastProp.LastModified.Time.Before(time.Now())).To(BeTrue())
				Expect(lastProp.LastModified.Time.Add(time.Minute).After(time.Now())).To(BeTrue())
			})
			It("should have a recent LastModified", func() {
				Expect(lastProp.Lifecycle).To(Equal(model.PreProductionLifecycle))
			})
		}
		When("scope is specified", func() {
			BeforeEach(func() {
				scope = &userScope
			})
			AssertPropertyWasAdded()
			It("should get the specified scope", func() {
				Expect(lastProp.Scope).To(Equal(userScope))
			})
		})
		When("scope is not specified", func() {
			BeforeEach(func() {
				scope = nil
			})
			AssertPropertyWasAdded()
			It("should default to EventScope", func() {
				Expect(lastProp.Scope).To(Equal(eventScope))
			})
		})
	})
	When("re-adding a voided property", func() {
		BeforeEach(func() {
			modelTax.Properties = append(modelTax.Properties, modelProperty("foo", "Int32", eventScope, model.VoidLifecycle))
			apiTax.Properties = append(apiTax.Properties, apiProperty("foo", "Int32", &eventScope))
		})
		It("should re-appear as PreProduction", func() {
			Expect(modelTax.Properties[5].Lifecycle).To(Equal(model.PreProductionLifecycle))
		})
	})
	When("re-adding a ProductionEndOfLife property", func() {
		BeforeEach(func() {
			modelTax.Properties = append(modelTax.Properties, modelProperty("foo", "Int32", eventScope, model.ProductionEndOfLifeLifecycle))
			apiTax.Properties = append(apiTax.Properties, apiProperty("foo", "Int32", &eventScope))
		})
		It("should re-appear as Production", func() {
			Expect(modelTax.Properties[5].Lifecycle).To(Equal(model.ProductionLifecycle))
		})
	})
	When("removing a Production property", func() {
		When("is envelope property", func() {
			var envelopeProperty string = "event"
			BeforeEach(func() {
				apiTax.Properties = removeApiProperty(envelopeProperty, apiTax.Properties)
			})
			It("should not actually be removed", func() {
				Expect(findModelProperty(envelopeProperty, modelTax.Properties)).NotTo(BeNil())
			})
			It("should not change lifecycle to EndOfLife", func() {
				Expect(findModelProperty(envelopeProperty, modelTax.Properties).Lifecycle).To(Equal(model.ProductionLifecycle))
			})
		})
		When("is not envelope or internal property", func() {
			var publicProperty string = "user_id"
			BeforeEach(func() {
				apiTax.Properties = removeApiProperty(publicProperty, apiTax.Properties)
			})
			It("should not actually be removed", func() {
				Expect(findModelProperty(publicProperty, modelTax.Properties)).NotTo(BeNil())
			})
			It("should change lifecycle to EndOfLife", func() {
				Expect(findModelProperty(publicProperty, modelTax.Properties).Lifecycle).To(Equal(model.ProductionEndOfLifeLifecycle))
			})
		})
	})
	When("removing a PreProduction property", func() {
		BeforeEach(func() {
			findModelProperty("item_id", modelTax.Properties).Lifecycle = model.PreProductionLifecycle
			apiTax.Properties = removeApiProperty("item_id", apiTaxonomy1.Properties)
		})
		It("it should actually be removed", func() {
			Expect(findModelProperty("item_id", modelTax.Properties).Lifecycle).To(Equal(model.VoidLifecycle))
		})
	})
	When("modifying a property", func() {
		BeforeEach(func() {
			apiTax.Properties[0].Type = "Int32"
			apiTax.Properties[2].Scope = &eventScope
		})
		It("the changes should be reflected in the model", func() {
			Expect(modelTax.Properties[0].Type).To(Equal("Int32"))
			Expect(modelTax.Properties[2].Scope).To(Equal(eventScope))
		})
		It("still keeps internal properties living only in model", func() {
			Expect(findModelProperty("pipeline_internal", modelTax.Properties).Lifecycle).To(Equal(model.PreProductionLifecycle))
		})
	})
	When("adding an enum", func() {
		BeforeEach(func() {
			modelTax.BuildIndex()
			modelTax.GetEnum("Event").GetField("A").Lifecycle = model.ProductionEndOfLifeLifecycle
			apiTax.Enums = append(apiTax.Enums, Enum{Name: "ContentType", Fields: []EnumField{
				apiEnumField("FOO", nil),
				apiEnumField("BAR", nil),
			}})
			apiTax.Enums = append(apiTax.Enums, Enum{
				Name: "Event",
				Fields: []EnumField{
					apiEnumField("A", nil),
					apiEnumField("B", nil),
					apiEnumField("C", nil),
				},
			})
		})
		It("should actually be added", func() {
			Expect(modelTax.Enums).To(HaveLen(3))
			Expect(modelTax.Enums[2].Name).To(Equal("ContentType"))
		})
		It("should get continuous field values starting at 1", func() {
			for i, field := range modelTax.GetEnum("ContentType").Fields {
				Expect(field.Value).To(Equal(i + 1))
			}
		})
		It("where lifecycle is ProductionEndOfLife should Revive it", func() {
			Expect(modelTax.GetEnum("Event").GetField("A").Lifecycle).To(Equal(model.ProductionLifecycle))
		})
	})
	When("removing an enum", func() {
		BeforeEach(func() {
			apiTax.Enums = apiTax.Enums[1:]
		})
		It("should actually be removed", func() {
			Expect(modelTax.Enums).To(HaveLen(1))
			Expect(modelTax.Enums[0].Name).To(Equal(apiTax.Enums[0].Name))
		})
	})
	When("Event enum does not have value 4", func() {
		BeforeEach(func() {
			modelTax.Enums[0].Fields = append(modelTax.Enums[0].Fields, modelEnumField("E", 5))
		})
		When("adding two enum fields", func() {
			BeforeEach(func() {
				apiTax.Enums[0].Fields = append(apiTax.Enums[0].Fields, apiEnumField("New", nil))
				apiTax.Enums[0].Fields = append(apiTax.Enums[0].Fields, apiEnumField("Second", nil))
			})
			When("there is no twin", func() {
				It("picks 4 and 6 as the next values", func() {
					Expect(apiTax.Enums[0].Fields).To(HaveLen(5))
					Expect(modelTax.GetEnum("Event").GetField("New").Value).To(Equal(4))
					Expect(modelTax.GetEnum("Event").GetField("Second").Value).To(Equal(6))
				})
			})
			When("there is a twin that fills the 4 but does not have 6", func() {
				BeforeEach(func() {
					modelTaxTwin := modelTaxonomy1Twin.DeepCopy()
					modelTaxTwin.Enums[0].Fields = append(modelTaxTwin.Enums[0].Fields, modelEnumField("G", 7))
					apiTax.twin = modelTaxTwin
				})
				It("picks 6 as the next value", func() {
					Expect(apiTax.Enums[0].Fields).To(HaveLen(5))
					Expect(modelTax.GetEnum("Event").GetField("New").Value).To(Equal(6))
					Expect(modelTax.GetEnum("Event").GetField("Second").Value).To(Equal(8))
				})
			})
		})
	})
	When("no changes in taxonomy", func() {
		//var lastProp *model.EventProperty
		var kubeTaxonomy v1beta1.EventTaxonomy

		When("shuffling order in properties", func() {
			var enumZeroLifecycles map[string]model.Lifecycle = make(map[string]model.Lifecycle)

			BeforeEach(func() {
				rand.Shuffle(len(apiTax.Properties), func(i, j int) {
					apiTax.Properties[i], apiTax.Properties[j] = apiTax.Properties[j], apiTax.Properties[i]
				})
				for _, enum := range modelTax.Enums {
					for _, field := range enum.Fields {
						enumZeroLifecycles[field.Name] = field.Lifecycle
					}
				}
			})

			It("assert no changes happens in model", func() {
				Expect(modelTax.Properties[2].Name).To(Not(Equal(apiTax.Properties[2].Name)))
				for _, field := range modelTax.Enums[0].Fields {
					Expect(field.Lifecycle).To(Equal(enumZeroLifecycles[field.Name]))
				}

			})
			It("still keeps internal properties living only in model", func() {
				Expect(findModelProperty("pipeline_internal", modelTax.Properties).Lifecycle).To(Equal(model.PreProductionLifecycle))
			})
			It("deep copy of model into taxonomy again should assert no changes", func() {
				modelTax.DeepCopyInto(&kubeTaxonomy.Spec.Taxonomy)
				Expect(modelTax.Properties).To(HaveLen(len(kubeTaxonomy.Spec.Taxonomy.Properties)))
				Expect(reflect.DeepEqual(modelTax.Properties, kubeTaxonomy.Spec.Taxonomy.Properties)).To(BeTrue())
			})
		})
		When("shuffling order in enum properties", func() {
			var enumZeroLifecycles = make(map[string]model.Lifecycle)

			BeforeEach(func() {

				rand.Shuffle(len(apiTax.Enums[0].Fields), func(i, j int) {
					apiTax.Enums[0].Fields[i], apiTax.Enums[0].Fields[j] = apiTax.Enums[0].Fields[j], apiTax.Enums[0].Fields[i]
				})
				for _, enum := range modelTax.Enums {
					for _, field := range enum.Fields {
						enumZeroLifecycles[field.Name] = field.Lifecycle
					}
				}
			})
			It("assert no changes happens in model", func() {
				Expect(modelTax.Enums[0].Fields).To(HaveLen(len(apiTax.Enums[0].Fields)))
				Expect(modelTax.Enums[0].Fields[0].Name).ToNot(Equal(apiTax.Enums[0].Fields[0].Name))
				for _, field := range modelTax.Enums[0].Fields {
					Expect(field.Lifecycle).To(Equal(enumZeroLifecycles[field.Name]))
				}

			})
			It("still keeps internal properties living only in model", func() {
				Expect(findModelProperty("pipeline_internal", modelTax.Properties).Lifecycle).To(Equal(model.PreProductionLifecycle))
			})
			It("deep copy of model into taxonomy again should assert no changes", func() {
				// If above fails, this might pass.
				modelTax.DeepCopyInto(&kubeTaxonomy.Spec.Taxonomy)
				Expect(modelTax.Enums[0].Fields).To(HaveLen(len(kubeTaxonomy.Spec.Taxonomy.Enums[0].Fields)))
				Expect(reflect.DeepEqual(modelTax.Enums[0].Fields, kubeTaxonomy.Spec.Taxonomy.Enums[0].Fields)).To(BeTrue())
			})
		})

	})
})

func defaultToProduction(lifecycles []model.Lifecycle) model.Lifecycle {
	if len(lifecycles) > 0 {
		return lifecycles[0]
	}
	return model.ProductionLifecycle
}

func findModelProperty(name string, properties []*model.EventProperty) *model.EventProperty {
	for _, property := range properties {
		if property.Name == name {
			return property
		}
	}
	return nil
}

func modelProperty(name, typeStr string, scope model.PropertyScope, lifecycle ...model.Lifecycle) *model.EventProperty {
	return &model.EventProperty{
		Name:         name,
		Scope:        scope,
		Lifecycle:    defaultToProduction(lifecycle),
		LastModified: lastModYesterday,
		Type:         typeStr,
	}
}

func modelEnumField(name string, value int, lifecycle ...model.Lifecycle) *model.EventEnumField {
	return &model.EventEnumField{Name: name, Value: value, Lifecycle: defaultToProduction(lifecycle), LastModified: lastModYesterday}
}

func removeApiProperty(name string, properties []Property) []Property {
	n := 0
	for _, property := range properties {
		if property.Name != name {
			properties[n] = property
			n++
		}
	}
	properties = properties[:n]
	return properties
}

func apiProperty(name, typeStr string, scope *model.PropertyScope) Property {
	return Property{
		Name:  name,
		Type:  typeStr,
		Scope: scope,
	}
}

func apiEnumField(name string, value *int) EnumField {
	return EnumField{Name: name, Value: value}
}
