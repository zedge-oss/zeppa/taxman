// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package taxman

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/fgrosse/zaptest"
	"github.com/gorilla/mux"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/fake"
	typedclientset "gitlab.com/zedge-oss/zeppa/taxman/pkg/client/clientset/typed/dwh/v1beta1"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

const (
	fooAppID = "foo"
	barAppID = "bar"
)

var _ = Describe("Taxman handlers", func() {
	var (
		h             Handlers
		fakeClientSet *fake.Clientset
		fakeClient    typedclientset.EventTaxonomyInterface
		recorder      *httptest.ResponseRecorder
		request       *http.Request
		handler       http.Handler
		body          io.Reader
		uri           = "/taxonomy/foo"
		method        string
		vars          = map[string]string{"appid": ""}
	)
	BeforeEach(func() {
		fakeClientSet = fake.NewSimpleClientset(fooTaxonomy, barTaxonomy)
		fakeClient = fakeClientSet.DwhV1beta1().EventTaxonomies(fakeNamespace)
		h = Handlers{
			Namespace: fakeNamespace,
			Client:    fakeClient,
			Log:       zaptest.LoggerWriter(GinkgoWriter).Sugar(),
		}
	})
	When("handling PUT requests", func() {
		var response putTaxonomyResponse
		JustBeforeEach(func() {
			request = httptest.NewRequest(method, uri, body)
			recorder = httptest.NewRecorder()
			request = mux.SetURLVars(request, vars)
			handler = http.HandlerFunc(h.putTaxonomyHandler)
			handler.ServeHTTP(recorder, request)
		})
		When("PUT-ing a non-existing taxonomy", func() {
			BeforeEach(func() {
				vars["appid"] = "does-not-exist"
			})
			It("should return HTTP 404", func() {
				Expect(recorder.Result().StatusCode).To(Equal(http.StatusNotFound))
			})
		})
		When("PUT-ing an invalid taxonomy name", func() {
			var msg msgResponse
			BeforeEach(func() {
				vars["appid"] = ""
			})
			JustBeforeEach(func() {
				_ = json.NewDecoder(recorder.Result().Body).Decode(&msg)
			})
			It("should return HTTP 400", func() {
				Expect(recorder.Result().StatusCode).To(Equal(http.StatusBadRequest))
				Expect(msg.Message).To(Equal("could not extract appid"))
			})
		})
		When("PUT-ing an existing taxonomy", func() {
			When("the input is identical", func() {
				BeforeEach(func() {
					body = ioutil.NopCloser(bytes.NewBuffer(fooApiTaxonomyJson))
					vars["appid"] = fooAppID
				})
				It("should return HTTP 200", func() {
					Expect(recorder.Result().StatusCode).To(Equal(http.StatusOK))
				})
			})
			When("the input adds an enum field in conflict with the twin", func() {
				BeforeEach(func() {
					body = ioutil.NopCloser(bytes.NewBuffer(barApiTaxonomyJson))
					vars["appid"] = barAppID
				})
				JustBeforeEach(func() {
					_ = json.NewDecoder(recorder.Body).Decode(&response)
				})
				It("should fail with twin value/name mismatch", func() {
					Expect(recorder.Result().StatusCode).To(Equal(http.StatusBadRequest))
					Expect(response.Issues).To(HaveLen(1))
					Expect(response.Issues[0]).To(ContainSubstring("value/name mismatch with twin"))
				})
			})
		})
	})
})

var (
	lastModTime       = time.Unix(1573217753, 0)
	lastModDateString = lastModTime.Format("2006-01-02T15:04:05Z")
	lastMod           = metav1.NewTime(lastModTime)
	fooTaxonomy       = &v1beta1.EventTaxonomy{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fooAppID,
			Namespace: fakeNamespace,
		},
		Spec: v1beta1.EventTaxonomySpec{
			Taxonomy: model.EventTaxonomy{
				AppID: fooAppID,
				Properties: []*model.EventProperty{
					{Name: "time", Scope: model.InternalScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "DateTime"},
					{Name: "event", Scope: model.EventScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Enum:Event"},
					{Name: "userid", Scope: model.UserScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Int64"},
				},
				Enums: []*model.EventEnum{
					{Name: "Event", Fields: []*model.EventEnumField{
						{Name: "BROWSE", Value: 1, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
						{Name: "CLICK", Value: 2, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
					}},
				},
			},
		},
	}
	barTaxonomy = &v1beta1.EventTaxonomy{
		ObjectMeta: metav1.ObjectMeta{
			Name:      barAppID,
			Namespace: fakeNamespace,
		},
		Spec: v1beta1.EventTaxonomySpec{
			TwinTaxonomy: fooAppID,
			Taxonomy: model.EventTaxonomy{
				AppID: barAppID,
				Properties: []*model.EventProperty{
					{Name: "time", Scope: model.InternalScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "DateTime"},
					{Name: "event", Scope: model.EventScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Enum:Event"},
					{Name: "userid", Scope: model.UserScope, Lifecycle: model.ProductionLifecycle, LastModified: lastMod, Type: "Int64"},
				},
				Enums: []*model.EventEnum{
					{Name: "Event", Fields: []*model.EventEnumField{
						{Name: "BROWSE", Value: 1, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
					}},
				},
			},
		},
	}
	fooApiTaxonomyJson = []byte(`{
      "appid": "foo",
      "properties": [
        {"name": "time", "scope": "Internal", "type": "DateTime", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "event", "scope": "Event", "type": "Enum:Event", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "userid", "scope": "User", "type": "Int64", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
      ],
      "enums": [
        {"name": "Event", "fields": [
          {"name": "BROWSE", "value": 1, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
          {"name": "CLICK", "value": 2, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
        ]}
      ]
    }`)
	barApiTaxonomyJson = []byte(`{
      "appid": "bar",
      "properties": [
        {"name": "time", "scope": "Internal", "type": "DateTime", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "event", "scope": "Event", "type": "Enum:Event", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
        {"name": "userid", "scope": "User", "type": "Int64", "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
      ],
      "enums": [
        {"name": "Event", "fields": [
          {"name": "BROWSE", "value": 1, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"},
          {"name": "BUY", "value": 2, "lifecycle": "Production", "last_modified": "` + lastModDateString + `"}
        ]}
      ]
    }`)
)
